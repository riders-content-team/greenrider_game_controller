#!/usr/bin/env python

import rospy
from std_msgs.msg import String
import json
from gazebo_msgs.srv import GetModelState
from gazebo_msgs.msg import ModelState
from gazebo_msgs.srv import SetModelState
from std_srvs.srv import Trigger, TriggerResponse
from std_msgs.msg import Bool
import time
import math
import os
import requests
import subprocess
import atexit
from random import randint

class Vector3D:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
    def __add__(self, v):
        return Vector3D(self.x+v.x, self.y+v.y, self.z+v.z)
    def __sub__(self, v):
        return Vector3D(self.x-v.x, self.y-v.y, self.z-v.z)
    def __mul__(self, scalar):
        return Vector3D(self.x * scalar, self.y * scalar, self.z * scalar)
    def dot(self, v):
        return self.x * v.x + self.y * v.y + self.z * v.z
    def length(self):
        return math.sqrt(self.x * self.x + self.y * self.y + self.z * self.z)

class GameController:

    def __init__(self, robot_name):
        self.robot_name = robot_name
        #initializing point on the belt
        self.belt_init_pose = Vector3D(-1.57, -0.22, 0.89)
        
        green_box  = Vector3D(1.53, -1.07, 0.26)
        yellow_box = Vector3D(0.64, -1.83, 0.26)
        blue_box   = Vector3D(-0.24, -1.08, 0.26)
        self.boxes = [green_box,yellow_box,blue_box]

        model_names2 = []
        self.model_names = ['box1','box2','box3','box4','box5',
        'bottle1','bottle2','bottle3','bottle4','bottle5',
        'can1','can2','can3','can4','can5']
        model_names2 = self.model_names

        #initialize ros and subscriber
        rospy.init_node("game_controller")
        self.new_obj_sub = rospy.Subscriber('/' + self.robot_name + '/spawn_new_object', Bool, self.newObjHandler)
        self.new_obj_state = False

        self.obj_sub = rospy.Subscriber('/' + self.robot_name + '/contact', String, self.ObjHandler)
        self.obj_name = ""

        self.grip_sub = rospy.Subscriber('/' + self.robot_name + '/grip_state', Bool, self.gripperHandler)
        self.grip_state = False
        self.lap = 0

        self.correct = 0
        self.incorrect = 0
        self.status = "running"
        self.score = 0

        self.time_passed = 0.0
        self.start_time = 0.0

        metric_msg_rate = 10
        rate = rospy.Rate(metric_msg_rate)

        self.metric_pub = rospy.Publisher('simulation_metrics', String, queue_size=10)

        self.metrics = {
            'Status' : self.status,
            'Time' : self.time_passed,
            'Correct': self.correct,
            'Incorrect': self.incorrect,
            'Score':self.score
        }

        self.git_dir = '/workspace/src/.git'
        commit_id = ''
        riders_project_id = os.environ.get('RIDERS_PROJECT_ID', '')

        # RIDERS_COMMIT_ID
        git_dir_exists = os.path.isdir(self.git_dir)
        if git_dir_exists:

            popen = subprocess.Popen(['git', 'remote', 'get-url', 'origin'], cwd=self.git_dir, stdout=subprocess.PIPE)
            remote_url, error_remote_url = popen.communicate()

            popen = subprocess.Popen(['git', 'name-rev', '--name-only', 'HEAD'], cwd=self.git_dir, stdout=subprocess.PIPE)
            branch_name, error_branch_name = popen.communicate()

            popen = subprocess.Popen(['git', 'rev-parse', 'HEAD'], cwd=self.git_dir, stdout=subprocess.PIPE)
            commit_id, error_commit_id = popen.communicate()

            if error_remote_url is None and remote_url is not None:
                remote_url = remote_url.rstrip('\n').rsplit('@')
                user_name = remote_url[0].rsplit(':')[1][2:]
                remote_name = remote_url[1]

        self.result_metrics = {
            'Status' : self.status,
            'Correct' : self.correct,
            'Incorrect': self.incorrect,
            'Score': self.score,
            'Time' : self.time_passed,
            'commit_id': commit_id.rstrip('\n'),
        }

        self.init_services()

        self.send_api_check = False
        self.is_finish = False

        correct_num = 10
        prev_model = 0

        while ~rospy.is_shutdown():
            if(len(self.model_names) <= 2):
                self.model_names = ['box1','box2','box3','box4','box5',
                    'bottle1','bottle2','bottle3','bottle4','bottle5',
                    'can1','can2','can3','can4','can5']
            self.time_passed = round(rospy.get_time() - self.start_time,2)

            self.publish_metrics()
        
            if(self.new_obj_state):
                rand_num = randint(0,len(self.model_names)-1)
                self.state_msg = ModelState()
                self.state_msg.model_name = self.model_names[rand_num]
                self.state_msg.pose.position.x = self.belt_init_pose.x
                self.state_msg.pose.position.y = self.belt_init_pose.y
                self.state_msg.pose.position.z = self.belt_init_pose.z
                self.state_msg.pose.orientation.x = 0
                self.state_msg.pose.orientation.y = 0
                self.state_msg.pose.orientation.z = 0
                self.state_msg.pose.orientation.w = 0
                try:
                    rospy.wait_for_service("/gazebo/set_model_state", 1.0)
                    self.set_model_state = rospy.ServiceProxy('/gazebo/set_model_state', SetModelState)
                    resp = self.set_model_state(self.state_msg)
                except (rospy.ServiceException, rospy.ROSException), e:
                    self.robot_status = "no_robot"
                    rospy.logerr("Service call failed: %s" % (e,))

                self.new_obj_state=False
                self.model_names.pop(rand_num)
            if(self.correct == 10 and not self.is_finish): #?
                self.status = "Finished"
                self.result_metrics['Score'] = self.score * 1000
                self.result_metrics['Correct'] = self.correct
                self.result_metrics['Incorrect'] = self.incorrect
                self.result_metrics['Status'] = self.status
                self.result_metrics['Time'] = self.time_passed
                self.publish_metrics()
                self.send_to_api(self.score, False, **self.result_metrics)
                self.is_finish = True
            try:
                rate.sleep()
            except(rospy.exceptions.ROSTimeMovedBackwardsException), e:
                print(e)

    def ObjHandler(self, msg):
        self.obj_name = msg.data.split(':',1)[0]
        print(self.obj_name)

    def init_services(self):
        try:
            rospy.wait_for_service("/gazebo/get_model_state", 1.0)
            self.get_robot_state = rospy.ServiceProxy("/gazebo/get_model_state", GetModelState)
            
            resp = self.get_robot_state("greenrider", "")
            if not resp.success == True:
                print("no robot")

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))
    
    def newObjHandler(self,state):
        if(state.data):
            self.new_obj_state = True
        else:
            self.new_obj_state = False
    
    def gripperHandler(self,state):
        #print(state.data)
        if(not state.data):
            try:
                rospy.wait_for_service("/gazebo/get_model_state", 1.0)
                self.get_model_state = rospy.ServiceProxy("/gazebo/get_model_state", GetModelState)
                resp = self.get_model_state(self.obj_name, "")
                for i in range(3):
                    diff_x = abs(resp.pose.position.x - self.boxes[i].x)
                    diff_y = abs(resp.pose.position.y - self.boxes[i].y)
                    print(diff_x)
                    if(( (diff_x <= 0.5 and diff_y <= 0.24 and i== 1) or (diff_x <= 0.24 and diff_y <= 0.35) ) and not self.is_finish):
                        #i=0 => green
                        #i=1 => yellow
                        #i=2 => blue
                        print(self.obj_name + "     " + str(i))
                        if(i==0 and "bottle" in self.obj_name):
                            print("bottle")
                            self.correct +=1
                        elif(i==1 and "box" in self.obj_name):
                            print("box")
                            self.correct +=1
                        elif(i==2 and "can" in self.obj_name):
                            print("can")
                            self.correct +=1
                        else:
                            self.incorrect +=1
                        if(not self.is_finish):
                            self.score = self.correct/self.time_passed

                if not resp.success == True:
                    print("no robot")

            except (rospy.ServiceException, rospy.ROSException), e:
                rospy.logerr("Service call failed: %s" % (e,))
    
    def achieve(self, achievement_id):
        # get parameters from environment
        host = os.environ.get('RIDERS_HOST', None)
        project_id = os.environ.get('RIDERS_PROJECT_ID', None)
        token = os.environ.get('RIDERS_AUTH_TOKEN', None)

        # generate request url
        achievement_url = '%s/api/v1/project/%s/achievements/%s/achieve/' % (host, project_id, achievement_id)

        # send request
        rqst = requests.post(achievement_url, headers={
            'Authorization': 'Token %s' % token,
            'Content-Type': 'application/json',
        })
    
    def publish_metrics(self):
        self.metrics['Status'] = self.status
        self.metrics['Time'] = self.time_passed # round(self.time_passed, 2)
        self.metrics['Correct'] = self.correct
        self.metrics['Incorrect'] = self.incorrect
        self.metrics['Score'] = self.score * 1000

        self.metric_pub.publish(json.dumps(self.metrics, sort_keys=True))
    
    def send_to_api(self, score=0.0, disqualified=False,  **kwargs):
        simulation_id = os.environ.get('RIDERS_SIMULATION_ID', None)
        args = score, disqualified
        try:
            if simulation_id:
                self.send_simulation_results(simulation_id, *args, **kwargs)
            else:
                self.send_branch_results(*args, **kwargs)
        except Exception as e:
            rospy.loginfo('Exception occurred while sending metric: %s', e)
    
    def send_simulation_results(self, simulation_id, score, disqualified, **kwargs):
        host = os.environ.get('RIDERS_HOST', None)
        token = os.environ.get('RIDERS_SIMULATION_AUTH_TOKEN', None)
        create_round_url = '%s/api/v1/simulation/%s/rounds/' % (host, simulation_id)
        kwargs['score'] = kwargs.get('score', score)
        kwargs['disqualified'] = kwargs.get('disqualified', disqualified)
        data = {
            'metrics': json.dumps(kwargs)
        }
        # send actual data
        requests.post(create_round_url, {}, data, headers={
            'Authorization': 'Token %s' % token,
            'Content-Type': 'application/json',
        })
    
    def send_branch_results(self, score, disqualified, **kwargs):
        rsl_host = 'http://localhost:8059'
        # metrics endpoint stores data differently compared to simulation endpoint,
        # hence we change how we send it
        info = {
            'score': score,
            'disqualified': disqualified,
        }
        info.update(kwargs)
        url = '%s/events' % rsl_host
        data = {
            'event': 'metrics',
            'info': json.dumps(info)
        }

        requests.post(url, {}, data, headers={
            'Content-Type': 'application/json',
        })


